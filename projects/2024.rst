.. _gsoc-2024-projects:

:far:`calendar-days` 2024
##########################


Enhanced Media Experience with AI-Powered Commercial Detection and Replacement
********************************************************************************

.. youtube:: Kagg8JycOfo
   :width: 100%

| **Summary:** Leveraging the capabilities of BeagleBoard’s powerful processing units, the project will focus on creating a real-time, efficient solution that enhances media consumption experiences by seamlessly integrating custom audio streams during commercial breaks.

- Develop a neural network model: Combine Convolutional Neural Networks (CNNs) and Recurrent Neural Networks (RNNs) to analyze video and audio data, accurately identifying commercial segments within video streams.
- Implement a real-time pipeline: Create a real-time pipeline for BeagleBoard that utilizes the trained model to detect commercials in real-time and replace them with alternative content or obfuscate them, alongside replacing the audio with predefined streams.
- Optimize for BeagleBoard: Ensure the entire system is optimized for real-time performance on BeagleBoard hardware, taking into account its unique computational capabilities and constraints.


**Contributor:** Aryan Nanda

**Mentors:** `Jason Kridner <https://forum.beagleboard.org/u/jkridner>`_, `Deepak Khatri <https://forum.beagleboard.org/u/lorforlinux>`_, Kumar Abhishek

.. grid:: 2 2 2 2

   .. grid-item::

      .. button-link:: https://summerofcode.withgoogle.com/archive/2024/projects/UOX7iDEU
         :color: info
         :shadow:
         :expand:

         :fab:`google;pst-color-light` - GSoC Registry
   
   .. grid-item::

      .. button-link:: https://gsoc.beagleboard.io/proposals/old/2024/commercial_detection_and_replacement.html
         :color: primary
         :shadow:
         :expand:

         Proposal


Low-latency I/O RISC-V CPU core in FPGA fabric
************************************************

.. youtube:: ic0RRK6d3hg
  :width: 100%

| **Summary:** Implementation of PRU subsystem on BeagleV-Fire’s FPGA fabric, resulting in a real-time microcontroller system working alongside the main CPU, providing low-latency access to I/O.

**Contributor:** Atharva Kashalkar

**Mentors:** `Cyril Jean <https://forum.beagleboard.org/u/vauban>`_, `Jason Kridner <https://forum.beagleboard.org/u/jkridner>`_, Vedant Paranjape, Kumar Abhishek

.. grid:: 2 2 2 2

   .. grid-item::

      .. button-link:: https://summerofcode.withgoogle.com/archive/2024/projects/KjUoFlg2
         :color: info
         :shadow:
         :expand:

         :fab:`google;pst-color-light` - GSoC Registry
   
   .. grid-item::

      .. button-link:: https://gsoc.beagleboard.io/proposals/old/2024/RISC-V_CPU_core_in_FPGA_fabric.html
         :color: primary
         :shadow:
         :expand:

         Proposal

Differentiable Logic for Interactive Systems and Generative Music - Ian Clester
************************************************

.. youtube:: NvHxMCF8sAQ
  :width: 100%

| **Summary:** Developing an embedded machine learning system on BeagleBoard that leverages Differentiable Logic (DiffLogic) for real-time interactive music creation and environment sensing. The system will enable on-device learning, fine-tuning, and efficient processing for applications in new interfaces for musical expression.

**Contributor:** Ian Clester

**Mentors:** `Jack Armitage <https://forum.beagleboard.org/u/jarm/summary>`_, Chris Kiefer

.. grid:: 2 2 2 2

   .. grid-item::

      .. button-link:: https://summerofcode.withgoogle.com/archive/2024/projects/FBk0MM8g
         :color: info
         :shadow:
         :expand:

         :fab:`google;pst-color-light` - GSoC Registry
   
   .. grid-item::

      .. button-link:: https://gsoc.beagleboard.io/proposals/old/2024/ijc.html
         :color: primary
         :shadow:
         :expand:

         Proposal