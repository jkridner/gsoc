:html_theme.sidebar_primary.remove: true
:sd_hide_title: true

.. _gsoc-project-ideas:

Ideas
######

.. image:: ../_static/images/ideas-below.webp
   :align: center

.. admonition:: How to participate?

   Contributors are expected to go through the list of ideas dropdown below and join the discussion by clicking on the
   ``Discuss on forum`` button. All ideas have colorful badges for ``Complexity`` and
   ``Size`` for making the selection process easier for contributors. Anyone is welcome to
   introduce new ideas via the `forum gsoc-ideas tag <https://forum.beagleboard.org/tag/gsoc-ideas>`_.
   Only ideas with sufficiently experienced mentor backing as deemed by the administrators will
   be added here.

   +------------------------------------+-------------------------------+
   | Complexity                         | Size                          |
   +====================================+===============================+
   | :bdg-danger:`High complexity`      | :bdg-danger-line:`350 hours`  |
   +------------------------------------+-------------------------------+
   | :bdg-success:`Medium complexity`   | :bdg-success-line:`175 hours` |
   +------------------------------------+-------------------------------+
   | :bdg-info:`Low complexity`         | :bdg-info-line:`90 hours`     |
   +------------------------------------+-------------------------------+

.. card:: 

   :fas:`microchip;pst-color-primary` Demo project idea (Topic of project here. Refer :ref:`GSoC-old-ideas`) :bdg-success:`Medium complexity` :bdg-success-line:`175 hours` (update these tags accordingly)

   ^^^^
   **Project heading**

   Project Description here 

   | **Goal:** Goal of your project here
   | **Hardware Skills:** eg. `Verilog`_, `verification`_, `FPGA`_
   | **Software Skills:** eg. `RISC-V ISA`_, `assembly`_, `Linux`_
   | **Possible Mentors:** eg. `Cyril Jean <https://forum.beagleboard.org/u/vauban>`_, `Jason Kridner <https://forum.beagleboard.org/u/jkridner>`_

   ++++

   .. button-link:: Link to project discussion forum thread here 
      :color: danger
      :expand:

      :fab:`discourse;pst-color-light` Discuss on forum

.. button-link:: https://forum.beagleboard.org/tag/gsoc-ideas
   :color: danger
   :expand:
   :outline:

   :fab:`discourse;pst-color-light` Visit our forum to see newer ideas being discussed!


.. tip::
   
   You can also check our :ref:`GSoC-old-ideas` and :ref:`Past_Projects` for inspiration.

.. _C:
   https://jkridner.beagleboard.io/docs/latest/intro/beagle101/c.html

.. _Assembly:
   https://jkridner.beagleboard.io/docs/latest/intro/beagle101/assembly.html

.. _Verilog:
   https://jkridner.beagleboard.io/docs/latest/intro/beagle101/verilog.html

.. _Zephyr:
   https://jkridner.beagleboard.io/docs/latest/intro/beagle101/zephyr.html

.. _Linux:
   https://docs.beagleboard.org/latest/intro/beagle101/linux.html

.. _device-tree:
   https://jkridner.beagleboard.io/docs/latest/intro/beagle101/device-tree.html

.. _FPGA:
   https://jkridner.beagleboard.io/docs/latest/intro/beagle101/fpga.html

.. _basic wiring:
   https://jkridner.beagleboard.io/docs/latest/intro/beagle101/basic-wiring.html

.. _motors:
   https://jkridner.beagleboard.io/docs/latest/intro/beagle101/motors.html

.. _embedded serial interfaces:
   https://jkridner.beagleboard.io/docs/latest/intro/beagle101/embedded-serial.html

.. _OpenBeagle CI:
   https://jkridner.beagleboard.io/docs/latest/intro/beagle101/openbeagle-ci.html

.. _verification:
   https://jkridner.beagleboard.io/docs/latest/intro/beagle101/verification.html

.. _wireless communications:
   https://jkridner.beagleboard.io/docs/latest/intro/beagle101/wireless-communications.html

.. _Buildroot:
   https://jkridner.beagleboard.io/docs/latest/intro/beagle101/buildroot.html

.. _RISC-V ISA:
   https://jkridner.beagleboard.io/docs/latest/intro/beagle101/riscv.html
