.. _gsoc-ideas-rtos-microkernel-improvements:

RTOS/microkernel imporvements
#############################

These project ideas focus on growing open source for microcontrollers with a 
specific focus on better understanding of computer systems and co-processing 
in a heterogeneous, asymmetrical multiprocessor system where Linux is the 
typical kernel used on the system coordination portion of the system. 

.. card:: Upstream Zephyr Support on BeagleBone AI-64 R5

   :fas:`timeline;pst-color-secondary` RTOS/microkernel imporvements :bdg-success:`Medium complexity` :bdg-danger-line:`350 hours`
   
   ^^^^

   Incorporating Zephyr RTOS support onto the Cortex-R5 cores of the TDA4VM SoC along with Linux operation on the A72 core. The objective is to harness the combined capabilities of both systems
   to support BeagleBone AI-64.

   - **Goal:** submit upstream patches to support BeagleBone AI-64 and respond to feedback
   - **Hardware Skills:** Familiarity with ARM Cortex R5
   - **Software Skills:** `C`_, `RTOS  <https://docs.zephyrproject.org/latest/develop/getting_started/index.html>`_
   - **Possible Mentors:** `Dhruva Gole <https://forum.beagleboard.org/u/dhruvag2000>`_, `Nishanth Menon <https://forum.beagleboard.org/u/nishanth_menon>`_
   - **Upstream Repository:** `The primary repository for Zephyr Project <https://github.com/zephyrproject-rtos/zephyr>`_
   - **Expected Size of Project:** 350 hrs
   - **Rating:** Medium
   - **References:**
      - `J721E TRM <http://www.ti.com/lit/pdf/spruil1>`_
   ++++

   .. button-link:: https://forum.beagleboard.org/t/upstream-zephyr-support-on-bbai-64-r5/37294/1
      :color: danger
      :expand:

      :fab:`discourse;pst-color-light` Discuss on forum

.. card:: 

    :fas:`timeline;pst-color-secondary` **RTEMS on RISC-V** :bdg-success:`Medium priority` :bdg-success:`Medium complexity` :bdg-danger-line:`Large size` 
    ^^^^

    - **Goal:** Add RISC-V-based PolarFire SoC support to RTEMS RTOS
    - **Hardware Skills:** RISC-V
    - **Software Skills:** C, RTOS
    - **Possible Mentors:** Joel Sherrill, jkridner
    - **Expected Size of Project:** 350 hrs
    - **Rating:** Medium
    - **Upstream Repository:** `https://git.rtems.org <https://git.rtems.org/>`_
    - **References:**
        - `Issue on RTEMS tracker <https://devel.rtems.org/ticket/4626>`_

.. card:: 

    :fas:`timeline;pst-color-secondary` **Zephyr on R5/M4F (K3)** :bdg-success:`Medium priority` :bdg-success:`Medium complexity` :bdg-danger-line:`Large size` 
    ^^^^

    - **Goal:** Add Zephyr RTOS support to the R5/M4F cores in the J721E/AM62 SoC
    - **Hardware Skills:** R5/M4F
    - **Software Skills:** C, RTOS
    - **Possible Mentors:** NishanthMenon, Vaishnav Achath
    - **Expected Size of Project:** 350 hrs
    - **Rating:** Medium
    - **Upstream Repository:** https://github.com/zephyrproject-rtos/zephyr
    - **References:**
        - `J721E TRM <http://www.ti.com/lit/pdf/spruil1>`_
        - `AM62 TRM <https://www.ti.com/lit/pdf/spruiv7>`_

.. _C:
   https://jkridner.beagleboard.io/docs/latest/intro/beagle101/c.html