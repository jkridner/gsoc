.. _gsoc-idea-soft-peripherals-using-co-processors:

Soft peripherals using co-processors
####################################

The programmable real-time units are one of the coolest features of BeagleBone. Of course, 
the architecture of using a main processor running a high-level operating system and 
co-processors running without an operating system is a general architecture. Projects 
that help create tools and learning around this architecture can be rather successful 
around BeagleBone. See https://beagleboard.org/pru.

- low-latency audio (bela.io) (see more projects below)
- LED lighting (Falcon Player, PocketScroller, etc.)

Another approach is to add support for Verilog (or VHDL) to synthesis PRU code for the soft 
peripherals. This can be based on the open source verilog toolchains. 

.. card:: 

    :fab:`uncharted;pst-color-secondary` **Add features to simpPRU**
    ^^^^

    simpPRU is a procedural programming language. It is a statically typed language. 
    Variables and functions must be assigned data types during compilation. It is 
    type safe, and data types of variables are decided during compilation. It has 
    been a project that is being worked on from two years now, this is the third 
    year and there are certain features that could augment it further.

    Additionally, this needs to be benchmarked against vanilla PRU-C code to see the 
    performance comparison between the two. Ring Test: https://pub.pages.cba.mit.edu/ring/ 
    is a good benchmarking test to measure the latency between actions and the call from 
    the processor. There are many more benchmarks that can be run on PRU to compare 
    performance of simpPRU against vanilla PRU C code. The findings can be published 
    as a paper, just like this one: https://ieeexplore.ieee.org/document/8056744

    - **Goals:**
        - Improve the features of simpPRU, namely:
            - add test.py to GitHub actions
            - indicator for PRU on/off state in simppru-console - Issue issue `#24 <https://github.com/VedantParanjape/simpPRU/issues/24>`_
            - quality of life improvements, such as improving error handling. Suggestions for more features by contributors are welcome.
            - add support for calling C code directly into simpPRU
            - BONUS: support for using PWM using eCAP module: analog_write() and analog_read() functions
        - Benchmark the effects of adding various compiler flags (-O2, -O3) to gcc-pru during compilation of the generated C code
        - Document the benchmark results as a research paper, and also help the mentors to write a paper regarding simpPRU and other PRU benchmarks.
        - If time permits, start porting the code at https://github.com/beagleboard/librobotcontrol that performs balancing on a 2-wheel robot using a lag-lead controller and moving the control loop over to the PRUs using simpPRU
    - **Hardware Skills:** Minimal
    - **Software Skills:** C, Linux, Compilers
    - **Possible Mentors:** Vedant Paranjape, Abhishek, Archisman
    - **Expected Size of Project:** 350 hrs
    - **Rating:** Medium
    - **Upstream Repository:** https://github.com/VedantParanjape/simpPRU/
    - **References:** 
        - https://github.com/VedantParanjape/simpPRU/wiki#things-to-be-implemented-in-future
        - https://github.com/VedantParanjape/simpPRU/wiki#known-issues
        - https://github.com/VedantParanjape/simpPRU/issues/5

    ++++

    :bdg-success:`Medium priority` :bdg-success:`Medium complexity` :bdg-danger-line:`Large size`

.. card:: 

    :fab:`uncharted;pst-color-secondary` **Using dma-buf for PRU data transfers**

    ^^^^

    Today, to make data transfers to and from the PRU (if using remoteproc, rpmsg dirvers), 
    1 PRU is utilized to make the transfers, which could instead be used to perform other 
    real time tasks. This project is intended to solve this problem using dma-buf, to relieve 
    the PRU, CPU of data transfer tasks.

    - **Goal:** Provide a way for users to use dma-buf for PRU data transfers. Dma-buf is to be based on OpenAMP, and ION allocator. Additional stretch goals for the project are to add this functionality to PRUSS-Bindings to make it a complete API to operate the PRUs; and possibly integrating with SimpPRU.
    - **Complexity:** 350 hours
    - **Hardware Skills:** Minimal
    - **Software Skills:** C, basics of kernel modules
    - **Possible Mentors:** Pratim Ugale, giuliomoro
    - **Rating:** Easy
    - **Upstream Repository:** 
        - TBA (New Repo), for integrating:
            - https://github.com/pratimugale/PRUSS-Bindings
            - https://github.com/VedantParanjape/simpPRU
    - **References:** 
        - https://static.linaro.org/connect/bkk19/presentations/bkk19-207.pdf
        - `Previous PRU-DMA project for reference (currently in unstable state) <https://github.com/maciejjo/beaglebone-pru-dma>`_

    ++++

    :bdg-success:`Medium priority` :bdg-info:`Low complexity` :bdg-danger-line:`Large size`

.. card:: 

    :fab:`uncharted;pst-color-secondary` **Port am335x_pru_package to remoteproc**

    ^^^^

    The am335x_pru_package is a community supported set of tools and examples on using the PRU. 
    It includes an assembler and a C library for managing the PRU over UIO. The BeagleBoard 
    community mostly desires to migrate PRU development to `remoteproc <https://www.kernel.org/
    doc/Documentation/remoteproc.txt>`_ to give the Linux kernel greater knowledge of the cores.

    - **Goal:** Move all examples to remoteproc, including adding elf support to pasm, adding an option to build using clpru and updating the C library to use the remoteproc interface for basic PRU control and using mmap() for a limited set of communications functions.
    - **Hardware Skills:** N/A
    - **Software Skills:** C, assembly, Linux kernel
    - **Possible Mentors:** Kumar Abhishek, Zubeen Tolani, Andrew Henderson, giuliomoro
    - **Expected Size of Project:** 350 hrs
    - **Rating:** Medium
    - **Upstream Repository:** https://github.com/beagleboard/am335x_pru_package
    - **References:** 
        - `v5 examples <https://git.ti.com/pru-software-support-package>`_ 
        - `TI Training Labs <http://processors.wiki.ti.com/index.php/PRU_Training:_Hands-on_Labs>`_

    ++++

    :bdg-success:`Medium priority` :bdg-success:`Medium complexity` :bdg-danger-line:`Large size`
  
.. card:: 

    :fab:`uncharted;pst-color-secondary` **Create a PRU based soft-peripheral example**
    ^^^^

    The PRU is a hard realtime co-processor with dedicated realtime pins. Previous GSoC 
    have created an assortment of examples and foundation pieces. However, most of them 
    have custom interfaces. This project is to tie things together to implement at least 
    one example of using the PRU with a standard interface. For example, this can be a 
    SPI master interface that can be bound with another peripheral via DT.

    - **Goal:** Create at least one non trivial example that includes both the PRU firmware and the kernel driver interface. Utilization of remoteproc (other means of loading the firmware will be considered provided it is appropriately justified) to load the firmware and control the PRU. A device driver should expose the functionality using standard kernel APIs. The example should NOT be a trivial one (i.e. GPIO controller, simple input events, plain on-off LEDs) unless proper justification is provided. Simple non trivial possible examples include SPI master, I2C master (potential HW issues), or UART. This project includes creating the relevant PRU firmware.
    - **Hardware Skills:** Some understanding of I/O is needed esp if the I2C master is choosen.
    - **Software Skills:** C, assembly, Linux kernel
    - **Possible Mentors:** Kumar Abhishek, Zubeen Tolani, giuliomoro, Andrew Henderson
    - **Expected Size of Project:** 350 hrs
    - **Rating:** Medium
    - **Upstream Repository:** N/A
    - **References:** BeagleLogic & BeagleScope from previous years.

    ++++

    :bdg-success:`Medium priority` :bdg-success:`Medium complexity` :bdg-danger-line:`Large size`

.. card:: 

    :fab:`uncharted;pst-color-secondary` **PRU-based USB stack**
    ^^^^

    It should be possible to write a USB stack for PRU. The PRU-controlled USB interface 
    should be able to work either as device or a host. The goal would be to minimize the 
    latency for USB peripherals, specifically audio, HID and MIDI devices used for musical 
    instruments, to integrate into the Bela.io software environment.

    - **Possible Mentors:** Jason Kridner, giuliomoro

    ++++

    :bdg-success:`Medium priority` :bdg-success:`Medium complexity` :bdg-danger-line:`Large size`
    
.. card:: 

    :fab:`uncharted;pst-color-secondary` **GPIO Benchmarking**
    ^^^^

    Goal: Read about the ring oscillator. Benchmark GPIOs using the below four approaches
        - mmap() - userspace
        - character driver (libgpiod) - userspace
        - sysfs - userspace
        - kernel driver

    Answer and document questions like
        - How fast can we access pins into {memory, Filesys}?
        - How fast can we output a signal?

    If possible come up with a use case for fast GPIO with or without PRU.

    - **Complexity:** 175 hours
    - **Hardware Skills:** Minimal
    - **Software Skills:** Linux, C, kernel modules
    - **Possible Mentors:** Jason Kridner, Shreyas Atre
    - **Rating:** Easy
    - **Upstream Repository:** TBD
    - **References:** 
        - https://pub.pages.cba.mit.edu/ring/
        - https://gitlab.cba.mit.edu/pub/ring
        - https://github.com/ehayon/BeagleBone-GPIO

    ++++

    :bdg-info:`Low priority` :bdg-info:`Low complexity` :bdg-success-line:`Medium size`

.. card:: 

    :fab:`uncharted;pst-color-secondary` **bb-config improvements**
    ^^^^

    `bb-config` aka `beaglecfg` aka `beagle-config` is a terminal UI to make the configuration of 
    beaglebone devices easier and more visually. Since bullseye is the default Debian of beaglebone 
    boards we should take advantage of the changes brought into it and improve the functionalities.

    - **Goal:**
        - Support for network-manager using dbus API. Talking directly to dbus saves a step and may expose a richer interface than the command line.
        - Adding a graph section for ADC/DAC using FTXUI graph
        - Solve the following Issues (Highest priority to the lowest):
            - Update to the latest FTXUI Version
            - `Bullseye (generic-sys-mods) regenerate_ssh_host_keys <https://github.com/SAtacker/beagle-config/issues/37>`_
            - `Add support for setting custom pinmux <https://github.com/SAtacker/beagle-config/issues/42>`_
            - `Add support for Overlay File System enable/disable <https://github.com/SAtacker/beagle-config/issues/32>`_
            - Optional (depending upon the time and priorities) `eMMC stats regression <https://github.com/SAtacker/beagle-config/issues/26>`_
            - command line parity with rpi-config
            - Improve the GitHub CI/CD

    - **Complexity:** 175 hours
    - **Hardware Skills:** Minimal
    - **Software Skills:** Linux, C, C++, kernel modules
    - **Rating:** Medium
    - **Upstream Repository:** https://github.com/SAtacker/beagle-config
    - **References:** 
        - `bb-config <https://github.com/SAtacker/beagle-config>`_
        - `NM Dbus spec <https://developer-old.gnome.org/NetworkManager/stable/spec.html>`_
        - `Dbus CXX <https://dbus-cxx.github.io/>`_
        - `NM DBus examples <http://cheesehead-techblog.blogspot.com/2012/09/dbus-tutorial-fun-with-network-manager.html>`_

    ++++

    :bdg-success:`Medium priority` :bdg-success:`Medium complexity` :bdg-success-line:`Medium size`

.. card:: 

    :fab:`uncharted;pst-color-secondary` **remoteproc tty**
    ^^^^

    - **Goal:** Write a new driver to model mailbox and other IPC schemes with a tty/virt terminal interface emulation - allowing for logs and debug messages to be accessible via standard interfaces. Initial implementation can be loopback, but could involve standardization to help implementation across heterogeneous processors. Complexity: 350 hours
    - **Hardware Skills:** Minimal(understand IPC)
    - **Software Skills:** Linux, C, C++, kernel modules
    - **Possible Mentors:** NishanthMenon
    - **Rating:** Medium
    - **Upstream Repository:** https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/tree/
    - **References:** 
        - `remoteproc <https://www.kernel.org/doc/Documentation/remoteproc.txt>`_
        - `tty <https://www.kernel.org/doc/html/latest/tty/index.html>`_ 

    ++++

    :bdg-success:`Medium priority` :bdg-success:`Medium complexity` :bdg-danger-line:`Large size`
