.. _2024-gsoc-proposals:

2024 Archived Proposals
########################

This directory contains archived proposals and related assets from 2024.

.. toctree::
    :maxdepth: 1
    :caption: Accepted Proposals

    melta101
    ijc
    commercial_detection_and_replacement
    RISC-V_CPU_core_in_FPGA_fabric

.. toctree::
    :maxdepth: 1
    :caption: Other Proposals

    suraj-sonawane
    upstream_zephyr_bbai64_r5
    alecdenny
    matt-davison
    mc
    himanshuk
    drone_cape_for_beagle-v-fire
