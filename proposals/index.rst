Proposals
#########

.. tip:: 

    Checkout :ref:`gsoc-project-ideas` page to explore ideas and :ref:`gsoc-proposal-guide` page to write your own proposal. You can also refer to :ref:`old-gsoc-proposals` to view old proposals submitted for GSoC.

.. toctree:: 
    :maxdepth: 1
    :caption: Proposal template

    template

.. toctree:: 
    :maxdepth: 1
    :caption: Old Proposals

    old
